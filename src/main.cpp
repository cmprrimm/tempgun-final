#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include "main.h"
#include <Adafruit_MLX90614.h>
#include <DHT.h>

int triggerPin = 2;
bool backResult;
bool objTempResult;
bool ambTempResult;
bool humLevelResult;
bool wifiBtnResult;
int val;

const char *SSID = "VM4344806";
const char *PASS = "h3skPcxwhd6q";

unsigned long previousMillis;

#define DHTPIN 14
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
Adafruit_MLX90614 mlx = Adafruit_MLX90614();

void setup()
{

  pinMode(22, OUTPUT);       // Clock goes from master to periph
  pinMode(21, INPUT_PULLUP); // internally pulled up
  pinMode(triggerPin, INPUT);
  Wire.begin(21, 22, 10000);
  mlx.begin();
  LITTLEFS.begin();
  tft.begin();
  dht.begin();
  initDisplay();
  uint16_t calData[5] = {450, 3468, 330, 3489, 5};
  tft.setTouch(calData);
}

void getBackButtonPress()
{

  uint16_t t_x = 0, t_y = 0; // touch coords
  boolean pressed = tft.getTouch(&t_x, &t_y);

  if (pressed && backButton.contains(t_x, t_y))
  {

    page = 0;
    tft.fillScreen(TFT_BLACK);
    objTempResult = false;
    ambTempResult = false;
    humLevelResult = false;
    wifiBtnResult = false;
    initDisplay();
  }
  else
  {

    backButton.press(false);
  }
}

void getObjTempButtonPress()
{
  uint16_t t_x = 0, t_y = 0;
  boolean pressed = tft.getTouch(&t_x, &t_y);

  if (pressed && objTemp.contains(t_x, t_y))
  {
    objTemp.press(true);
    page = 1;
    tft.fillScreen(TFT_BLACK);
    drawBackBtn(true);
  }
  else
  {
    objTemp.press(false);
  }
}

void getAmbButtonPress()
{
  uint16_t t_x = 0, t_y = 0;
  boolean pressed = tft.getTouch(&t_x, &t_y);

  if (pressed && ambTemp.contains(t_x, t_y))
  {

    ambTemp.press(true);
    page = 2;
    tft.fillScreen(TFT_BLACK);
    ambTempResult = true;
    drawBackBtn(true);
  }
  else
  {
    ambTemp.press(false);
  }
}

void getHumButtonPress()
{
  uint16_t t_x = 0, t_y = 0;
  boolean pressed = tft.getTouch(&t_x, &t_y);

  if (pressed && humTemp.contains(t_x, t_y))
  {

    humTemp.press(true);
    page = 3;
    tft.fillScreen(TFT_BLACK);
    humLevelResult = true;
    drawBackBtn(true);
  }
  else
  {
    humTemp.press(false);
  }
}

void getWIFIButtonPress()
{
  uint16_t t_x = 0, t_y = 0;
  boolean pressed = tft.getTouch(&t_x, &t_y);

  if (pressed && wifiBtn.contains(t_x, t_y))
  {
    
    wifiBtn.press(true);
    page = 4;
    tft.fillScreen(TFT_BLACK);
    wifiBtnResult = true;
    drawBackBtn(true);
  }
  else
  {
    wifiBtn.press(false);
  }
}

void trigPress()
{
  int val = digitalRead(triggerPin);
  if (val == HIGH)
  {
    tft.setCursor(60, 120);
    tft.setTextSize(2);
    tft.setTextColor(TFT_YELLOW, TFT_BLACK);
    tft.print("Object = ");
    tft.print(mlx.readObjectTempC());
    tft.print("c");
    tft.println();
  }
}

void wifiConnect()
{
}

void menuSys()
{

  switch (page)
  {
  case 0:
    getHumButtonPress();
    getObjTempButtonPress();
    getAmbButtonPress();
    getWIFIButtonPress();
    break;

  case 1:
    menuHeader();
    tft.println("Object Temp");
    trigPress();
    getBackButtonPress();
    break;

  case 2:
    menuHeader();
    tft.println("Ambient Temp");
    tft.setCursor(60, 120);
    tft.setTextSize(2);
    tft.setTextColor(TFT_YELLOW, TFT_BLACK);
    tft.print("Ambient = ");
    tft.print(mlx.readAmbientTempC());
    tft.print("c");
    tft.println();
    getBackButtonPress();
    break;

  case 3:
    menuHeader();
    tft.println("Humid Level");
    tft.setCursor(60, 120);
    tft.setTextSize(2);
    tft.setTextColor(TFT_YELLOW, TFT_BLACK);
    tft.print("Humidity = ");
    tft.print(dht.readHumidity());
    tft.print("c");
    tft.println();
    getBackButtonPress();
    break;

  case 4:
    WiFi.begin(SSID, PASS);
    menuHeader();
    tft.println("Connect WIFI");
    tft.setTextSize(2);
    tft.setTextColor(TFT_YELLOW, TFT_BLACK);
    tft.println();
    tft.setCursor(60, 70);
    tft.print("SSID : ");
    tft.println(SSID);
    while (WiFi.status() != WL_CONNECTED)
    {
      tft.setCursor(40, 100);
      tft.print("My IP :");
      tft.println(WiFi.localIP());
    }
    getBackButtonPress();
    break;
  }
}

void loop(void)

{
  menuSys();
}
