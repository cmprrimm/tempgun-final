#include <HTTPClient.h>
#include <HardwareSerial.h>
#include <IPAddress.h>
#include <WString.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiType.h>
#include "TFT_eSPI.h"
#include <LITTLEFS.h>
#include "version.h"


TFT_eSPI tft = TFT_eSPI();
int page;
#define BUTTON_W 30
#define BUTTON_H 26

#define OBJTEMP_BUTTON_X 80
#define OBJTEMP_BUTTON_Y 60

#define AMBTEMP_BUTTON_X 200
#define AMBTEMP_BUTTON_Y 60

#define HUMTEMP_BUTTON_X 80
#define HUMTEMP_BUTTON_Y 110

#define WIFI_BUTTON_X 200
#define WIFI_BUTTON_Y 110

#define BACK_BUTTON_X 145
#define BACK_BUTTON_Y 193


// #define SERIAL_BAUD 115200
// const char *HOST = "www.google.co.uk";

TFT_eSPI_Button objTemp, backButton, ambTemp, humTemp, wifiBtn;

bool startInternals;

void drawObjTempBtn(bool invert)
{

	objTemp.drawButton(invert);
}

void drawBackBtn(bool invert)
{

	backButton.drawButton(invert);
}

void drawAmbBtn(bool invert)
{

	ambTemp.drawButton(invert);
}

void drawHumBtn(bool invert)
{

	humTemp.drawButton(invert);
}

void drawWifiBtn(bool invert)
{

	wifiBtn.drawButton(invert);
}
void menuHeader()
{
	tft.setTextSize(3);
	tft.setCursor(50, -5);
	tft.fillRect(1, 1, 318, 43, TFT_RED);
	tft.setTextColor(TFT_YELLOW);
}



void Menu()
{

	page = 0;
	tft.setRotation(3);

	tft.fillScreen(TFT_BLACK);
	menuHeader();
	//Title
	tft.setTextSize(2);
	tft.setCursor(50, 10);
	tft.setTextColor(TFT_YELLOW, TFT_RED);
	tft.println("AWESOMETHERMO");

	// Version
	tft.setTextSize(1);
	tft.setTextFont(2);
	tft.setCursor(0, 0);
	tft.setRotation(0);
	tft.print(VERSION_SHORT);
	tft.setRotation(1);

	// project detials
	tft.setTextSize(1);
	tft.setTextColor(TFT_YELLOW, TFT_BLACK);
	tft.setRotation(3);
	tft.setCursor(70, tft.height() - 20);
	tft.println("Robert Rimmer - 6130COMP");

while(WiFi.status() == WL_CONNECTED) {
	tft.setTextSize(1);
	tft.setTextColor(TFT_YELLOW, TFT_BLACK);
	tft.setRotation(3);
	tft.setCursor(260, tft.height() - 20);
	tft.println("WIFI");
}
	//drawBackBtn(true);
	drawObjTempBtn(true);
	drawAmbBtn(true);
	drawHumBtn(true);
	drawWifiBtn(true);
}

void initDisplay()
{

	objTemp.initButtonUL(
		&tft, OBJTEMP_BUTTON_X, OBJTEMP_BUTTON_Y, BUTTON_W, BUTTON_H, TFT_YELLOW, TFT_YELLOW, TFT_RED, (char *)"ObjTemp", 2);
	ambTemp.initButtonUL(
		&tft, AMBTEMP_BUTTON_X, AMBTEMP_BUTTON_Y, BUTTON_W, BUTTON_H, TFT_YELLOW, TFT_YELLOW, TFT_RED, (char *)"AmbTemp", 2);
	humTemp.initButtonUL(
		&tft, HUMTEMP_BUTTON_X, HUMTEMP_BUTTON_Y, BUTTON_W, BUTTON_H, TFT_YELLOW, TFT_YELLOW, TFT_RED, (char *)"HumLevel", 2);
	wifiBtn.initButtonUL(
		&tft, WIFI_BUTTON_X, WIFI_BUTTON_Y, BUTTON_W, BUTTON_H, TFT_YELLOW, TFT_YELLOW, TFT_RED, (char *)"WIFI", 2);
	backButton.initButtonUL(
		&tft, BACK_BUTTON_X, BACK_BUTTON_Y, BUTTON_W, BUTTON_H, TFT_YELLOW, TFT_YELLOW, TFT_RED, (char *)"Back", 2);

	Menu();
}
