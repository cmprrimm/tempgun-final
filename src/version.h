#ifndef BUILD_NUMBER
  #define BUILD_NUMBER ".5"
#endif
#ifndef VERSION
  #define VERSION "v0.5 alpha" 
#endif
#ifndef VERSION_SHORT
  #define VERSION_SHORT "v0.5a"
#endif